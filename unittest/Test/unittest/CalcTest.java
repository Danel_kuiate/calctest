package unittest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.daryl.unitproject.Calculatrice;

class CalcTest {

	@Test
	void test() {
		assertEquals("erreur addition()",4, Calculatrice.addition(2, 3));
		assertEquals("erreur addition()",-3, Calculatrice.addition(-1, -2));
	}
	
	public void testSoustraction() {
		assertEquals("erreur soustraction()",0, Calculatrice.soustraction(2, 2));
	}
	
	public void testMultiplication() {
		assertEquals("erreur multiplication()",4, Calculatrice.multiplication(2, 2));
	}

}
